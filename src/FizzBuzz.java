public class FizzBuzz {

    public void fizzBuzz(int number) {
        for (int i = 0; i < number; i++) {
            if (i % 3 == 0 & i % 5 != 0) {
                System.out.println(i + " Fizz");
            } else if (i % 5 == 0 & i % 3 != 0) {
                System.out.println(i + " Buzz");
            } else if (i % 3 == 0 & i % 5 == 0) {
                System.out.println(i + " FizzBuzz");
            }
        }
    }

    public static void main(String[] args) {

        FizzBuzz fb = new FizzBuzz();
        fb.fizzBuzz(50);
    }
}
