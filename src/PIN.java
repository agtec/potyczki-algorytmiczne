import java.util.ArrayList;
import java.util.List;

public class PIN {

    /*
    Bajtek zapomniał hasła do swojego telefonu. Pamięta on, że składało się ono z trzech różnych dodatnich
    liczb całkowitych a < b < c oddzielonych haszami. Dodatkowo suma tych liczb wynosiła n oraz dla każdej
    pary liczb (spośród (a, b), (a, c) oraz (b, c)) jedna z liczb była wielokrotnością drugiej.
    Pomóż mu policzyć ile możliwych trójek musi sprawdzić, aby mógł zdecydować, czy warto marnować
    na to czas, czy lepiej kupić nowy telefon. */

    // https://sio2.mimuw.edu.pl/c/pa-2018-1/p/pin/

    int a;
    int b;
    int c;

    public void findNumberOfValidCombinations(int n) {

        // a nie moze byc wieksze niz 1/3 sumy skoro b jest jej wielokrotnoscia, a c - wielokrotnoscia b;

        int max = n / 3;

        // szukam wszytskich dzielnikow i dodaje je do listy;

        List<Integer> dividers = new ArrayList<Integer>();

        for (int i = 1; i < max; i++) {
            if (n % i == 0) {
                dividers.add(i);
            }
        }

        for (Integer divider : dividers) {
            System.out.print(divider + " ");
        }
        System.out.println();

        int foundCounter = 0;


        for (int i = 0; i < dividers.size(); i++) {
            a = dividers.get(i);
            b = a + a;
            c = n - a - b;
            if (b % a == 0 && c % b == 0 && c < n && c > b && a + b + c == n) {
                foundCounter++;
                System.out.println("a: " + a + ", b: " + b + ", c: " + c);
            }
            int d = b;
            int e = c;
            while (d < e && e < n) {
                d += a;
                e = n - a - d;
                if (d % a == 0 && e % d == 0 && e > d && a + d + e == n) {
                    foundCounter++;
                    System.out.println("a: " + a + ", b: " + d + ", c: " + e);
                }
            }
            int f = b;
            int g = c;
            while (f < g && g < n) {
                g += f;
                if (g % f == 0 && g > f && g < n && a + f + g == n) {
                    foundCounter++;
                    System.out.println("a: " + a + ", b: " + f + ", c: " + g);
                }
            }
        }

        System.out.println("The number of valid PIN numers is: " + foundCounter);
    }

    public static void main(String[] args) {

        PIN pin = new PIN();

        pin.findNumberOfValidCombinations(35);
        pin.findNumberOfValidCombinations(1000);

    }
}
